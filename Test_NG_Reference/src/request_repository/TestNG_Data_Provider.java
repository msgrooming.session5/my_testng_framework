package request_repository;

import org.testng.annotations.DataProvider;

public class TestNG_Data_Provider {
	@DataProvider()
	public Object [][] Post_Request_Body_Data_Provider(){
		return new Object[][]
				{
					{"Tanmay", "CEO"},
					{"Arvind", "Manager"},
					{"Ashwini","TechLead"},
					{"Samir","SRQA"}
				};
				
	}
	@DataProvider()
	public Object [][] Put_Request_Body_Data_Provider(){
		return new Object[][]
				{
					{"Tanmay", "CEO"},
					{"Arvind", "Manager"},
					{"Ashwini","TechLead"},
					{"Samir","SRQA"}
				};
				
	}
	@DataProvider()
	public Object [][] Patch_Request_Body_Data_Provider(){
		return new Object[][]
				{
					{"Tanmay", "CEO"},
					{"Arvind", "Manager"},
					{"Ashwini","TechLead"},
					{"Samir","SRQA"}
				};
				
	}
}
