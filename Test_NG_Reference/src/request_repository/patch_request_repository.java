package request_repository;

import java.io.IOException;
import java.util.ArrayList;

import utility_common_methods.excel_data_extractor;

public class patch_request_repository {

	public static String patch_request() throws IOException {
		ArrayList<String> Data = excel_data_extractor.Excel_data_reader("test_data", "PATCH_API", "patch_tc2");
		String name = Data.get(1);
		String job = Data.get(2);
		String requestbody = "{\r\n" + "    \"name\": \"" + name + "\",\r\n" + "    \"job\": \"" + job + "\"\r\n" + "}";
		return requestbody;
	}

}
