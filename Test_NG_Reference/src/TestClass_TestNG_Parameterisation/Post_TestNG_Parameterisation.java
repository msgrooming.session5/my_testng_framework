package TestClass_TestNG_Parameterisation;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import API_common_methods.common_method_handle_API;
import Test_package.post_TC1;
import endpoint_repository.post_endpoint_repository;
import io.restassured.path.json.JsonPath;
import utility_common_methods.handle_api_logs;
import utility_common_methods.handle_directory;

public class Post_TestNG_Parameterisation extends common_method_handle_API {

	static File log_dir;
	static String requestBody;
	static String endpoint;
	static String responseBody;

	@DataProvider()
	public Object [][] Post_Request_Body(){
		return new Object[][]
				{
					{"Tanmay", "CEO"},
					{"Arvind", "Manager"},
					{"Ashwini","TechLead"},
					{"Samir","SRQA"}
				};
				
	}
	@BeforeTest
	public static void Test_Setup() throws IOException {
		log_dir = handle_directory.create_log_directory("post_TC1_logs");
		endpoint = post_endpoint_repository.post_endpoint();
	}

	@Parameters({"Req_name","Req_job"})
	@Test()
	public static void post_executor(String Req_name, String Req_job) throws IOException {
		requestBody ="{\r\n"
				+ "    \"name\": \""+Req_name+"\",\r\n"
				+ "    \"job\": \""+Req_job+"\"\r\n"
				+ "}";
		for (int i = 0; i < 5; i++) {
			int statusCode = post_statusCode(requestBody, endpoint);
			System.out.println("post API triggered");
			System.out.println(statusCode);
			if (statusCode == 201) {
				responseBody = post_responseBody(requestBody, endpoint);
				System.out.println(responseBody);

				post_TC1.validator(requestBody, responseBody);
				break;
			} else {
				System.out.println("post API expected status code 201 not found hence re-trying it");
			}
		}
	}

	public static void validator(String requestBody, String responseBody) {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 4);
		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_id = jsp_res.getString("id");
		String res_job = jsp_res.getString("job");
		String res_createdate = jsp_res.getString("createdAt");
		res_createdate = res_createdate.substring(0, 4);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdate, expecteddate);
	}

	@AfterTest
	public static void Test_Teardown() throws IOException {
		handle_api_logs.evidence_creator(log_dir, "post_TC1", endpoint, requestBody, responseBody);
	}
}
