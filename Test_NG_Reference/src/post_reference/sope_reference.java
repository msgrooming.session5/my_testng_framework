package post_reference;

import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class sope_reference {

	public static void main(String[] args) {
		// step 1 - declare the base url
		RestAssured.baseURI = "https://www.dataaccess.com";

		// step 2 - declare the request body
		String requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
				+ "   <soapenv:Header/>\r\n" + "   <soapenv:Body>\r\n" + "      <web:NumberToDollars>\r\n"
				+ "         <web:dNum>100</web:dNum>\r\n" + "      </web:NumberToDollars>\r\n"
				+ "   </soapenv:Body>\r\n" + "</soapenv:Envelope>";

		// step 3 - Trigger the API and fetch the responsebody

		String responseBody = given().header("Content-Type", "text/xml; charset=utf-8").body(requestBody).when()
				.post("/webservicesserver/NumberConversion.wso").then().extract().response().getBody().asString();

		// Step 4 - print the response body
		System.out.println(responseBody);

		// Step 5 - Extract the response body parameter
		XmlPath Xml_res = new XmlPath(responseBody);
		String res_tag = Xml_res.getString("NumberToDollarsResult");
		System.out.println(res_tag);

		// Step 6 - validate the responsebody
		Assert.assertEquals(res_tag, "one hundred dollars");

	}

}