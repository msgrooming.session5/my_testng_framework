package post_reference;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class patch_reference {

	public static void main(String[] args) {
		// step 1 - declare base url

		RestAssured.baseURI = "https://reqres.in/";
		// step 2 configure the request parameters and trigger the API
		String reqbody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		String resbody = given().header("Content-Type", "application/json").body(reqbody).when().patch("api/users/2")
				.then().extract().response().asString();

		System.out.println("Responsebody is :" + resbody);
		// step 3 create an object of json path to parse the request body and the response body
		JsonPath jsp_req = new JsonPath(reqbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expectedDate = currentdate.toString().substring(0, 8);
		JsonPath jsp_res = new JsonPath(resbody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_updatedAt = jsp_res.getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0, 8);

		// step 4 validate response body
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, expectedDate);

	}

}
